<?php

namespace App\Console\Commands;

use App\Components\Graph;
use Illuminate\Console\Command;

class ParseXml extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:xml {filepath} {start?} {end?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parses external XML';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Парсинг XML файла с выходами автобусов
     *
     * @return mixed
     */
    public function handle()
    {
        $startTime = $this->argument('start');
        $endTime = $this->argument('end');
        $file = file_get_contents($this->argument('filepath'));
        $raspvariant = new \SimpleXMLElement($file);

        $graphs = [];

        foreach ($raspvariant as $item) {
            $itemAttr = $item->attributes();
            $num = (int) $itemAttr['num'];
            $smena = (int) $itemAttr['smena'];

            if (!isset($graphs[$num])) {
                $graphs[$num] = new Graph($num);
            }
            $graphs[$num]->addSmena($smena);

            foreach ($item as $event) {
                $eventAttrs = $event->attributes();
                $eventId = (int) $eventAttrs['ev_id'];

                if ($eventId === Graph::INDUSTRIAL_EVENT) {
                    $stops = [];
                    foreach ($event as $stop) {
                        $stopAttrs = $stop->attributes();
                        $stops[] = [
                            'time' => $stopAttrs['time'],
                            'external_id' => $stopAttrs['st_id']
                        ];
                    }
                    $graphs[$num]->addIndustrialEvent($eventAttrs['start'], $eventAttrs['end'], $stops, $smena);
                }
            }
        }

        foreach ($graphs as $g) {
            echo "Graph num: " . $g->getNum() . PHP_EOL;
            echo "Smenas: " . PHP_EOL;
            foreach ($g->getSmenas() as $sm) {
                echo $sm . PHP_EOL;
            }
            echo "Industrial count: " . $g->getIndustrialCount() . PHP_EOL;
            echo "Total industrial time: " . $g->getTotalIndustrialTime() . PHP_EOL;

            if (isset($startTime) && isset($endTime)) {
                echo "Stops in a time interval: " . PHP_EOL;

                $stops = $g->getStopsForInterval($startTime, $endTime);

                foreach ($stops as $key => $stop) {
                    $event = $key + 1;

                    foreach ($stop as $item) {
                        echo "Smena #" . $item['smena'] . "; Event #" . $event . ": " . $item['stopTime'] . PHP_EOL;
                    }

                    echo PHP_EOL;
                }
            }
            echo PHP_EOL . "======================" . PHP_EOL;
        }
    }
}
