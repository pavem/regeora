<?php

namespace App\Components;

/**
 * Класс хелпер для работы со временем
 * @package App\Components
 */
class Time
{
    /**
     * Возвращает время в секундах от начала дня
     *
     * @param string $time Временная метка в формате "HH:mm", например, "17:09"
     * @return int
     */
    public static function getInSeconds(string $time)
    {
        $parts = explode(":", $time);
        return (int) $parts[0] * 60 + (int) $parts[1];
    }
}
