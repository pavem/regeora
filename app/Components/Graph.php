<?php

namespace App\Components;

use App\StopPoints;

/**
 * Класс "выхода" автобуса из парка
 * @package App\Components
 */
class Graph
{
    const INDUSTRIAL_EVENT = 4;

    /**
     * @var int Номер выхода
     */
    private $num;

    /**
     * @var array Массив смен по заданному "выходу"
     */
    private $smenas = [];

    /**
     * @var array Массив производственных (event ev_id="4") рейсов
     */
    private $industrialEvents = [];

    /**
     * @param int $num Номер выхода
     */
    public function __construct(int $num)
    {
        $this->num = $num;
    }

    /**
     * Возвращает номер выхода
     *
     * @return int
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * Добавление смены в массив смен
     * @param int $smena
     */
    public function addSmena(int $smena)
    {
        $this->smenas[] = $smena;
        $this->sortSmenas();
    }

    /**
     * @return array Возвращает список смен
     */
    public function getSmenas()
    {
        return $this->smenas;
    }

    /**
     * Сортировка массива смен
     */
    private function sortSmenas()
    {
        sort($this->smenas);
    }

    /**
     * Добавление производственного рейса
     *
     * @param string $start Время начала рейса
     * @param string $end Время окончания рейса
     * @param array $stops Массив с остановками в рейсе
     * @param int $smena Идентификатор смены
     */
    public function addIndustrialEvent(string $start, string $end, array $stops, int $smena)
    {
        $this->industrialEvents[] = [
            'start' => $start,
            'end' => $end,
            'stops' => $stops,
            'smena' => $smena
        ];
    }

    /**
     * Получение количества индустриальных рейсов
     *
     * @return int
     */
    public function getIndustrialCount()
    {
        return count($this->industrialEvents);
    }

    /**
     * Получение общего количества затраченного времени на производственные рейсы
     *
     * @return int
     */
    public function getTotalIndustrialTime()
    {
        $totalTime = 0;

        foreach ($this->industrialEvents as $event) {
            $totalTime += Time::getInSeconds($event['end']) - Time::getInSeconds($event['start']);
        }

        return $totalTime;
    }


    /**
     * Получение остановок по заданному временному интервалу
     *
     * @param string $startTime Время начала временного интервала
     * @param string $endTime Время окончания временного интервала
     * @return array
     */
    public function getStopsForInterval(string $startTime, string $endTime)
    {
        $start = Time::getInSeconds($startTime);
        $end = Time::getInSeconds($endTime);
        $stops = [];

        foreach ($this->industrialEvents as $key => $event) {
            foreach ($event['stops'] as $stop) {
                $stopTime = $stop['time'];
                $stop = Time::getInSeconds($stopTime);
                if ($stop >= $start && $stop <= $end) {
                    $sp = StopPoints::where('external_id', $stop['external_id'])->first();
                    $stops[$key][] = [
                        'stopTime' => $stopTime,
                        'smena' => $event['smena'],
                        'name' => $sp ?? ''
                    ];
                }
            }
        }

        return $stops;
    }
}
