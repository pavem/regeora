# Raspvariant

## Требования

- PHP 7.0+
- MySQL 5.6+
- Composer

## Деплой

- Склонировать репозиторий
- Установить зависимости проекта: `composer install`
- `cp .env.example .env`
- Настроить соединение с БД в `.env`

## Запуск

- `php artisan parse:xml raspvariant.xml`
- `php artisan parse:xml raspvariant.xml 20:59 21:04`
